let mongoose = require("mongoose"),
  Schema = mongoose.Schema;
let logsInfoSchema = new Schema(
  {
    updated: { type: Date, default: Date.now },
    created: { type: Date, default: Date.now }
  },
  { strict: false },
  { collection: "logsDetails" }
);

let logsData = mongoose.model("logsDetails", logsInfoSchema);
logsInfoSchema.pre("save", function(next) {
  this.updated = new Date();
  next();
});
logsInfoSchema.pre("update", function(next) {
  this.updated = new Date();
  next();
});
logsInfoSchema.pre("findOneAndUpdate", function(next) {
  this.updated = new Date();
  next();
});

module.exports = logsData;
