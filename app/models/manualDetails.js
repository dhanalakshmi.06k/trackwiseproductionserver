let mongoose = require("mongoose"),
    Schema = mongoose.Schema;
let manualLogsInfoSchema = new Schema({
    updated: { type: Date, default: Date.now },
    created: { type: Date, default: Date.now }
}, { strict: false }, { collection: "manualLogsDetails" });

let manualData = mongoose.model("manualLogsDetails", manualLogsInfoSchema);
manualLogsInfoSchema.pre("save", function(next) {
    this.updated = new Date();
    next();
});
manualLogsInfoSchema.pre("update", function(next) {
    this.updated = new Date();
    next();
});
manualLogsInfoSchema.pre("findOneAndUpdate", function(next) {
    this.updated = new Date();
    next();
});

module.exports = manualData;