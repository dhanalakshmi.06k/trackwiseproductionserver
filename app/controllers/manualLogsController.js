var express = require("express"),
    router = express.Router(),
    mongoose = require("mongoose"),
    manualLogsDetailsModel = mongoose.model("manualLogsDetails");

module.exports = function(app) {
    app.use("/", router);
};

router.post("/addManualLogsDetails", function(req, res, next) {
    console.log(req.body);
    var manualDetails = new manualLogsDetailsModel(req.body);
    manualDetails.save(function(err, result) {
        if (err) {
            console.log("fuelDetailsPost failed: " + err);
        }
        res.send(result);
    });
});





router.post("/uploadManualLogsDetails/:assetId", function(req, res, next) {
    console.log(req.params.assetId);
    console.log("**********************");
    console.log("abcddsaf", req.body);

    let inputData = req.body;
    console.log("-----------> input data", inputData);
    let count = 0;
    for (let i = 0; i < inputData.length; i++) {
        console.log("-----------> inside for");
        inputData[i]["AssetID"] = req.params.assetId;
        var logsDetails = new logsDetailsModel(inputData[i]);
        manualLogsDetailsModel.save(function(err, result) {
            if (err) {
                console.log("uploadBulk file upload failed: " + err);
            } else {
                count += 1;
            }
        });
    }
    res.json({ "s": "successfully uploaded" });
});


router.get("/allManualLogs/count", function(req, res, next) {
    manualLogsDetailsModel.count(function(err, logsCount) {
        if (err) {
            res.status(500).send(err.message);
            logger.info(err.message);
        }
        var count = { count: logsCount };
        res.send(count);
    });
});

router.get("/allMannualLogs/:assetMongoId/:start/:range", function(req, res, next) {
    console.log("hiahjdfgdsj", req.params.assetMongoId)
    manualLogsDetailsModel
        .find({ AssetID: req.params.assetMongoId }, function(err, result) {
            if (err) {
                res.send(err);
                console.log(err.stack);
            } else {
                res.send(result);
            }
        })
        .skip(parseInt(req.params.start))
        .limit(parseInt(req.params.range));
});

router.delete("/AssetMannualByMongoId/:assetMongoId", function(req, res, next) {
    manualLogsDetailsModel
        .remove({ _id: req.params.assetMongoId }, function(err, result) {
            if (err) {
                console.log(err);
            } else {
                res.send(result);
            }
        })
        .sort({ created: 1 })
        .limit(1);
});
router.get("/getMannualAssetHealthDetails/:assetMongoId", function(req, res, next) {
    console.log("userMongoId", req.params.assetMongoId);
    manualLogsDetailsModel
        .find({ AssetId: req.params.assetMongoId }, function(err, result) {
            if (err) {
                console.log(err);
            } else {
                console.log(result);
                res.send(result);
            }
        })
        .limit(5);
});

router.get("/allMannualLogsByAssetId/:assetId/:start/:range", function(
    req,
    res,
    next
) {
    manualLogsDetailsModel
        .find({ AssetID: req.params.assetId }, function(err, result) {
            if (err) {
                res.send(err);
                console.log(err.stack);
            } else {
                res.send(result);
            }
        })
        .skip(parseInt(req.params.start))
        .limit(parseInt(req.params.range));
});

router.get("/allMannualLogsByAsssetIdAndParameter/:assetId/:parameterName", function(
    req,
    res,
    next
) {
    manualLogsDetailsModel.find({
            $and: [
                { AssetId: req.params.assetId },
                { VarName: req.params.parameterName }
            ]
        },
        function(err, result) {
            if (err) {
                res.send(err);
                console.log(err.stack);
            } else {
                res.send(result);
            }
        }
    );
});





router.get("/logsById/:logsMongoId", function(req, res, next) {
    console.log("userMongoId", req.params.logsMongoId);
    manualLogsDetailsModel.findOne({ _id: req.params.logsMongoId }, function(
        err,
        result
    ) {
        if (err) {
            console.log(err);
        } else {
            console.log(result);
            res.send(result);
        }
    });
});



router.post("/editLogsDetails/:logsMongoId", function(req, res, next) {
    manualLogsDetailsModel.findOneAndUpdate({ _id: req.params.logsMongoId },
        req.body, { upsert: true, new: true },
        function(err, result) {
            if (err) {
                console.log(err.stack);
            } else {
                res.send(result);
            }
        }
    );
});

router.delete("/logsByMongoId/:logsMongoId", function(req, res, next) {
    manualLogsDetailsModel.remove({ _id: req.params.logsMongoId }, function(
        err,
        result
    ) {
        if (err) {
            console.log(err);
        } else {
            res.send(result);
        }
    });
});