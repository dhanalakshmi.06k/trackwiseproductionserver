var express = require("express"),
    router = express.Router(),
    mongoose = require("mongoose"),
    logsDetailsModel = mongoose.model("logsDetails");

module.exports = function(app) {
    app.use("/", router);
};

router.post("/addLogsDetails", function(req, res, next) {
    var logsDetails = new logsDetailsModel(req.body);
    logsDetails.save(function(err, result) {
        if (err) {
            console.log("fuelDetailsPost failed: " + err);
        }
        res.send(result);
    });
});





router.post("/uploadLogsDetails/:assetId", function(req, res, next) {
    let inputData = req.body;
    let count = 0;
    for (let i = 0; i < inputData.length; i++) {
        inputData[i]["AssetID"] = req.params.assetId;
        var logsDetails = new logsDetailsModel(inputData[i]);
        logsDetails.save(function(err, result) {
            if (err) {
                console.log("uploadBulk file upload failed: " + err);
            } else {
                count += 1;
            }
        });
    }
    res.json({ "s": "successfully uploaded" });
});


router.get("/allLogs/count", function(req, res, next) {
    logsDetailsModel.count(function(err, logsCount) {
        if (err) {
            res.status(500).send(err.message);
            logger.info(err.message);
        }
        var count = { count: logsCount };
        res.send(count);
    });
});

router.get("/allLogs/:assetMongoId/:start/:range", function(req, res, next) {
    logsDetailsModel
        .find({ AssetID: req.params.assetMongoId }, function(err, result) {
            if (err) {
                res.send(err);
                console.log(err.stack);
            } else {
                res.send(result);
            }
        })
        .skip(parseInt(req.params.start))
        .limit(parseInt(req.params.range));
});

router.delete("/AssetByMongoId/:assetMongoId", function(req, res, next) {
    assetDetailsModel
        .remove({ _id: req.params.assetMongoId }, function(err, result) {
            if (err) {
                console.log(err);
            } else {
                res.send(result);
            }
        })
        .sort({ created: 1 })
        .limit(1);
});
router.get("/getAssetHealthDetails/:assetMongoId", function(req, res, next) {
    console.log("userMongoId", req.params.assetMongoId);
    logsDetailsModel
        .find({ AssetId: req.params.assetMongoId }, function(err, result) {
            if (err) {
                console.log(err);
            } else {
                console.log(result);
                res.send(result);
            }
        })
        .limit(5);
});

router.get("/allLogsByAsssetId/:start/:range/:assetId", function(
    req,
    res,
    next
) {
    logsDetailsModel
        .find({ AssetId: req.params.assetId }, function(err, result) {
            if (err) {
                res.send(err);
                console.log(err.stack);
            } else {
                res.send(result);
            }
        })
        .skip(parseInt(req.params.start))
        .limit(parseInt(req.params.range));
});

router.get("/allLogsByAsssetIdAndParameter/:assetId/:parameterName", function(
    req,
    res,
    next
) {
    logsDetailsModel.find({
            $and: [
                { AssetId: req.params.assetId },
                { VarName: req.params.parameterName }
            ]
        },
        function(err, result) {
            if (err) {
                res.send(err);
                console.log(err.stack);
            } else {
                res.send(result);
            }
        }
    );
});





router.get("/logsById/:logsMongoId", function(req, res, next) {
    console.log("userMongoId", req.params.logsMongoId);
    logsDetailsModel.findOne({ _id: req.params.logsMongoId }, function(
        err,
        result
    ) {
        if (err) {
            console.log(err);
        } else {
            console.log(result);
            res.send(result);
        }
    });
});



router.post("/editLogsDetails/:logsMongoId", function(req, res, next) {
    logsDetailsModel.findOneAndUpdate({ _id: req.params.logsMongoId },
        req.body, { upsert: true, new: true },
        function(err, result) {
            if (err) {
                console.log(err.stack);
            } else {
                res.send(result);
            }
        }
    );
});

router.delete("/logsByMongoId/:logsMongoId", function(req, res, next) {
    logsDetailsModel.remove({ _id: req.params.logsMongoId }, function(
        err,
        result
    ) {
        if (err) {
            console.log(err);
        } else {
            res.send(result);
        }
    });
});

router.get("/allLogsByTimeRange/:start/:end", function(req, res, next) {
    console.log(req.params.start);
    console.log(req.params.end);
    logsDetailsModel.find({
            $and: [
                { TimeString: { $gte: req.params.start } },
                { TimeString: { $lte: req.params.end } }
            ]
        },
        function(err, result) {
            if (err) {
                res.send(err);
                console.log(err.stack);
            } else {
                console.log(result);
                res.send(result);
            }
        }
    );
});

router.get("getAlarmsLogRecords/:column", function(req, res, next) {
    console.log(req.params.column);
    logsDetailsModel.find({ column: { $gt: req.params.start } },
        function(err, result) {
            if (err) {
                res.send(err);
                console.log(err.stack);
            } else {
                console.log(result);
                res.send(result);
            }
        }
    );
});


router.get("/getHealthDetails", function(
    req,
    res,
    next
) {
    logsDetailsModel.aggregate([{
                "$addFields": {
                    "TimeString": {
                        "$toDate": "$TimeString"
                    }
                }
            }],
            function(err, data) {

                if (err)
                    throw err;

                console.log(JSON.stringify(data, undefined, 2));

            }).sort({ "TimeString": -1 })
        .limit(5)
});



// logsDetailsModel
// .aggregate({
//     "$addFields": {
//         "TimeString": {
//             "$toDate": "$TimeString"
//         }
//     }
// }, function(err, result) {
//     if (err) {
//         res.send(err);
//         console.log(err.stack);
//     } else {
//         res.send(result);
//     }
// })
// .sort({ "TimeString": -1 })
// .limit(5);