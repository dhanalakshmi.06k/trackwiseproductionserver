/**
 * Created by suhas on 18/6/18.
 */

// let assetModel = require('../models/assets'),
    // assetConfigDao = require('../dao/assetConfig'),
    /*assetConfigDao = require('../dao/assetConfig'),
    constants=require('../../config/constants'),*/
    let mongoose=require('mongoose'),
    _=require("lodash");



let that ={};
that.getCount = function(type){
    return new Promise(function(resolve,reject){
        assetConfigDao.getAllTypes()
            .then(function (types) {
                console.log("***********")

                let assetTypeByConfig = _.keys(_.groupBy(types,'type'));
                if (type) {
                    assetTypeByConfig = _.intersection(assetTypeByConfig,type);
                }
                if(assetTypeByConfig.length>0){
                    assetModel.aggregate(
                        [
                            {
                                $match: {"assetType":{$in:assetTypeByConfig}}

                            },
                            {
                                $group: {_id : null,count:{$sum : 1}}
                            },
                            {
                                $project: {"count":1,"_id":0}
                            },

                        ],
                        function(err,count){
                            if(err){
                                reject(err)
                            }else{
                                if(count[0] && count[0]["count"]){
                                    resolve(count[0]["count"])
                                }else{
                                    resolve(0)
                                }

                            }
                        });
                }else{
                    reject("Asset Type Invalid")
                }
            })
    })

};

that.saveAsset = function(assetObj){
    return new Promise(function(resolve,reject){
        var assetModelInst = new assetModel(assetObj);
        assetModelInst.save(function(err,res){
            console.log("**************aftre seve88888888***************")
            console.log(res)
            if(err){
                console.log(err);
                reject(err);
            }else{
                resolve(res);

            }
        })
    })
};


that.getAllCountByTypes = function(){
    return new Promise(function(resolve,reject){
        assetModel.aggregate(
            [
                {
                    $group: {_id:"$assetType", count:{$sum:1}}

                },
                {
                    $project: {
                        "type":"$_id",
                        "count":1,
                        "_id":0
                    }
                },

            ],
            function(err,allTypeCounts){
                if(err){
                    reject(err)
                }else{
                    assetConfigDao.getAllTypes()
                        .then(function(allTypes){
                            let groupCountByType = _.groupBy(allTypeCounts,"type");
                            _.map(allTypes,function(typeObj){
                                if(groupCountByType[typeObj.type] && groupCountByType[typeObj.type][0]){
                                    typeObj.count=groupCountByType[typeObj.type][0].count;
                                }else{
                                    typeObj.count=0;
                                }
                            });
                            resolve(allTypes)
                        });
                }
            });
    });

};

that.getAssetById = function(id){
    return new Promise(function(resolve,reject){
        assetModel.findById(id,function(err,res){
            if(err){
                reject(err)
            }else{
                resolve(res);
            }
        })
    })
};

that.getAll = function(start,limit,order,type){
    return new Promise(function(resolve,reject){
        assetConfigDao.getAllTypes()
            .then(function(types){
                let assetTypeByConfig = _.keys(_.groupBy(types,'type'));
                if (type) {
                    assetTypeByConfig = _.intersection(assetTypeByConfig,type);
                }
                assetModel.find({})
                    .where('assetType')
                    .in(assetTypeByConfig)
                    .sort({'updated':-1})
                    .limit(limit)
                    .skip(start)
                    .exec(function(err,res){
                        if(err){
                            reject(err)
                        }else{
                            resolve(res);
                        }
                    });
            });
    })

};

that.deleteAsset = function(id){
    return new Promise(function(resolve,reject){
        assetModel.findByIdAndDelete(id,function(err,res){
            if(err){
                console.log(err);
                reject(err);
            }else{
                resolve(res);
            }
        })
    })
};

that.update = function(id,assetObj){
    return new Promise(function(resolve,reject){
        assetModel.findById(id, function (err, assetModelObj) {
            if (err){
                reject(err)
            }
            else{
                assetModelObj.set(assetObj);
                assetModelObj.save(function (err, updatedAsset) {
                    if (err) {
                        reject(err)
                    }
                    else{
                        resolve(updatedAsset);

                    }
                });
            }


        });
    })
};





module.exports=that;